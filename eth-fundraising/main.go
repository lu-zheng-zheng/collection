package main

import (
	"github.com/gin-gonic/gin"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"math/rand"
	"net/http"
	"path"
	"strconv"
	"time"
)

var db *gorm.DB

func init() {
	//创建一个数据库的连接
	var err error
	dsn := "root:root@tcp(127.0.0.1:3306)/eth-fundraising?charset=utf8mb4&parseTime=True&loc=Local"
	db, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		panic("failed to connect database")
	}

	db.AutoMigrate(&contractModel{})
}

func main() {

	router := gin.Default()

	v1 := router.Group("/api/v1/contract")
	{
		v1.POST("/", createContract)
		v1.GET("/blockChainId", findContract)
		v1.GET("/", findAllContract)

	}
	file := router.Group("/api/v1/file")
	{
		file.POST("/upload", Upload)
	}
	router.StaticFS("/static", http.Dir("./files"))

	router.Run()

}

func Upload(c *gin.Context) {
	//	获取文件
	file, err := c.FormFile("file")
	if err != nil {
		panic(err.Error())
		return
	}

	// 重新命名文件
	newFileName := strconv.FormatInt(time.Now().Unix(), 10) + strconv.Itoa(rand.Intn(999999-100000)+10000) + path.Ext(file.Filename)

	// 保存到服务器
	err = c.SaveUploadedFile(file, "./files/"+newFileName)
	if err != nil {
		c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "message": "文件上传失败！"})
		return
	}
	fileAddress := "/static/" + newFileName
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "url": fileAddress})

}

type (
	// todoModel 包括了 todoModel 的字段类型
	contractModel struct {
		gorm.Model
		BlockChainId    string `json:"blockChainId"`
		ContractAddress string `json:"contractAddress"`
	}
)

func createContract(c *gin.Context) {
	contract := contractModel{BlockChainId: c.PostForm("blockChainId"), ContractAddress: c.PostForm("contractAddress")}
	db.Save(&contract)
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "message": "Contract item created successfully!"})
}

func findContract(c *gin.Context) {
	var contract contractModel
	BlockChainId := c.Query("chainId")
	db.First(&contract, "block_chain_id = ?", BlockChainId)
	if contract.ContractAddress != "" {
		c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": contract})
	} else {
		c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "message": "No contract found!"})
	}

}

func findAllContract(c *gin.Context) {
	var contracts []contractModel
	db.Find(&contracts)
	if len(contracts) > 0 {
		c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": contracts})
	} else {
		c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "message": "No contract found!"})
	}
}
