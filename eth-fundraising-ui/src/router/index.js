import { createRouter, createWebHistory } from "vue-router"

export const realityRoutes = [
  {
    path: "/error",
    name: "Error",
    meta: {
			name: "错误",
			show: false,
		},
    component: () => import("/@/views/error.vue")
  },
  {
    path: "/",
    redirect: "/home",
    meta: {
			name: "首页",
			show: true,
      // 不显示父级菜单
      flat: true, 
		},
    component: () => import("/@/layout/index.vue"),
    children: [
      {
        path: "home",
        name: "Home",
        meta: {
          name: "首页",
          show: true,
        },
        component: () => import("/@/views/home.vue"),
      },
    ]
  },
  {
    path: "/fundrising",
    name: "Fundrising",
    redirect: "/fundrising/list",
    component: () => import("/@/layout/index.vue"),
    meta: {
      name: "募捐管理",
      show: true,
      flat: true,
    },
    children: [
      {
        path: "add",
        name: "FundrisingCreate",
        meta: {
          name: "发起募捐",
          show: true,
        },
        component: () => import("/@/views/fundrising/add.vue"),
      },
      {
        path: "list",
        name: "FundrisingList",
        meta: {
          name: "所有募捐列表",
          show: true,
          type: "all",
        },
        component: () => import("/@/views/fundrising/list.vue"),
      },
      {
        path: "initiator",
        name: "FundrisingList1",
        meta: {
          name: "我发起的",
          show: true,
          type: "initiator"
        },
        component: () => import("/@/views/fundrising/list.vue"),
      },
      {
        path: "donate",
        name: "FundrisingList2",
        meta: {
          name: "我捐赠的",
          show: true,
          type: "donate"
        },
        component: () => import("/@/views/fundrising/list.vue"),
      },
      {
        path: "detail",
        name: "FundrisingDetail",
        meta: {
          name: "募捐详情",
          show: false,
        },
        component: () => import("/@/views/fundrising/detail.vue"),
      
      }
      // {
      //   path: "detail",
      //   name: "FundrisingDetail",
      //   meta: {
      //     name: "募捐详情",
      //     show: false,
      //   },
      //   component: () => import("/@/views/fundrising/detail.vue"),
      // },
    ]
    
  }
]



const router = createRouter({
  history: createWebHistory(),
  routes: realityRoutes
})


export default router
