import * as ElementPlusIconsVue from '@element-plus/icons-vue';
import 'element-plus/dist/index.css';
import { createPinia } from 'pinia';
import 'virtual:uno.css';
import { createApp } from "vue";
import App from "./App.vue";
import './permission.js';
import router from './router';
import "./style.scss";
if (window.ethereum) {
  window.ethereum.on('accountsChanged', function (accounts) {
    // 当账号发生变化时，执行刷新页面的操作，或者你想要的其他操作
    window.location.reload(); // 这里示例使用刷新页面的方式
  });
  // 监听区块链网络变化事件
  window.ethereum.on('chainChanged', function (chainId) {
    // 当用户切换区块链网络时，执行刷新页面的操作，或者你想要的其他操作
    window.location.reload(); // 这里示例使用刷新页面的方式
  });
}
const app = createApp(App)
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component)
}
const pinia = createPinia()

app.use(router)
app.use(pinia)
app.mount('#app')


export default app
