import { defineStore } from 'pinia'


export const useMetamaskStore= defineStore('metamask',{
  state: () => ({
    status: 'unauthorized',
    account : '',
    fundrisingContract: null,
  }),
  getters: {
    getStatus: (state) => state.status,
    getAccount: (state) => state.account,
    getFundrisingContract: (state) => state.fundrisingContract,
  },
  actions: {
    setStatus(data) {
      this.status = data
    },
    setAccount(data) {
      this.account = data
    },
    setFundrisingContract(data) {
      this.fundrisingContract = data
    }
  }
})