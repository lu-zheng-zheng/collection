import { defineStore } from 'pinia'


export const useTokenStore= defineStore('token',{
  state: () => ({
    token: localStorage.getItem('token') || ''
  }),
  getters: {
    getToken: (state) => state.token,
  },
  actions: {
    setToken(data) {
      this.token = data
      localStorage.setItem('token', data)
    }
  }
})