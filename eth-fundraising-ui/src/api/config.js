import axios from 'axios';
import { ElMessage } from 'element-plus';

// 创建axios实例
const service = axios.create({
    baseURL: "/api/v1",
    // baseURL: "mock", // mock
    timeout: 15000 // 请求超时时间
})


export default service