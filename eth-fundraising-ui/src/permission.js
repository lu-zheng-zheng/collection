import detectEthereumProvider from '@metamask/detect-provider';
import Web3 from 'web3';
import router from './router';
import api from '/@/api/api';
import abi from '/@/assets/contract/fundraising.json';
import { useMetamaskStore } from '/@/store/metamask';

const whiteList = ['/login', '/register'];

router.beforeEach(async (to, from, next) => {
  const metamaskStore = useMetamaskStore()

  if (window.ethereum) {
    if (metamaskStore.getStatus == 'unauthorized') {
      console.log(metamaskStore.getStatus)
      try{
        const walletAddress =  await window.ethereum.enable()
        metamaskStore.setStatus('connected')
        console.log("当前钱包地址:" + walletAddress[0]);
        const provider = await detectEthereumProvider()
        if (provider) {
          console.log("provider", provider)
          console.log('Ethereum successfully detected!')
          const account = await provider.request({ method: 'eth_accounts' })
          console.log("account: ", account)
          metamaskStore.setAccount(account[0])
          // get chain id
          const chainId = await provider.request({ method: 'eth_chainId' })
          console.log("chainId: ", chainId)
          // chain id to 10 base
          const chainIdDecimal = parseInt(chainId, 16)
          const res = await api.contract.findByChainId({chainId: chainIdDecimal})
          console.log("res: ", res)
          if(res.data.data == null){
            next({path: '/error'})
            return
          }
          const contractAddress = res.data.data.contractAddress
          const web3js = new Web3(provider);
          const ethContract = new web3js.eth.Contract(abi,contractAddress)
          metamaskStore.setFundrisingContract(ethContract)
          next({path: '/'})
        } else {
          // if the provider is not detected, detectEthereumProvider resolves to null
          console.error('Please install MetaMask!', error)
          next({path: '/error'})
        }
      }catch(err){
        console.error(err)
        next({path: '/error'})
      }
    } else {
      next()
    }
  } else {
    if (whiteList.indexOf(to.path) !== -1) {
      // 在免登录白名单，直接进入
      next()
    } else {
      console.log("请安装metamask")
      next({path: '/error'})
    }
  }
  // if (tokenStore.getToken && tokenStore.getToken != '') {
  //   if (to.path == '/login') {
  //     next({ path: '/' })
  //   } else {
  //     next()
  //   }
  // } else {
  //   if (whiteList.indexOf(to.path) !== -1) {
  //     // 在免登录白名单，直接进入
  //     next()
  //   }else{
  //     console.log('token不存在')
  //     next({ path: '/login' })
  //   }
  // }
})

router.afterEach(() => { })
