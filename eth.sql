CREATE DATABASE `eth-fundraising`;
CREATE TABLE `contract_models`  (
  `block_chain_url` longtext CHARACTER SET utf8mb4  NULL,
  `contract_address` longtext CHARACTER SET utf8mb4  NULL,
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` datetime(3) NULL DEFAULT NULL,
  `updated_at` datetime(3) NULL DEFAULT NULL,
  `deleted_at` datetime(3) NULL DEFAULT NULL,
  `block_chain_id` longtext CHARACTER SET utf8mb4  NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_contract_models_deleted_at`(`deleted_at` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4  ROW_FORMAT = Dynamic;